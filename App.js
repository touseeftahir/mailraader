import React, {useState, useEffect} from 'react';
import {statusCodes, GoogleSignin} from '@react-native-community/google-signin';
import {Messages, SignIn} from './src/screens';
import {GOOGLE_CONFIG} from './constant';

GoogleSignin.configure(GOOGLE_CONFIG);

export default function App() {
  const [user, setUser] = useState(null);

  const login = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      setUser(userInfo);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('SignIn request canceled');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('SignIn request already in progress');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play service not available');
      } else {
        console.log(error);
      }
    }
  };

  const signOut = async () => {
    await GoogleSignin.signOut().then(() => {
      setUser(null);
    });
  };
  const checkLogin = async () => {
    if (GoogleSignin.isSignedIn) {
      const userInfo = await GoogleSignin.getCurrentUser();
      setUser(userInfo);
    }
  };

  useEffect(() => {
    checkLogin();
  }, []);

  return user ? (
    <Messages signOut={signOut} user={user} />
  ) : (
    <SignIn login={login} />
  );
}
