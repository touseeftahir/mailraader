import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {GoogleSigninButton} from '@react-native-community/google-signin';

export function SignIn({login}) {
  return (
    <View style={style.container}>
      <GoogleSigninButton
        style={{width: 222, height: 48}}
        size={GoogleSigninButton.Size.Wide}
        color={GoogleSigninButton.Color.Dark}
        onPress={login}
      />
      <Text>Not SignedIn</Text>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    minWidth: '100%',
    minHeight: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
