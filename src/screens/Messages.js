import React, {useState} from 'react';
import {useEffect} from 'react';
import {
  Text,
  View,
  Image,
  Button,
  StyleSheet,
  FlatList,
  RefreshControl,
} from 'react-native';
import moment from 'moment';

import {GoogleSignin} from '@react-native-community/google-signin';
import axios from 'axios';
import {API_KEY, GOOGLE_CONFIG, MESSAGE_URL} from '../../constant';
import {ListItem, ItemSeprator, Search} from '../components';
import {Colors} from 'react-native/Libraries/NewAppScreen';

GoogleSignin.configure(GOOGLE_CONFIG);
const CancelToken = axios.CancelToken;
const source = CancelToken.source();

export function Messages({user, signOut}) {
  const [nextPageToken, setNextPageToken] = useState(null);
  const [lastPage, setLastPage] = useState(false);
  const [messages, setMessages] = useState([]);
  const [loading, setLoading] = useState(true);
  const [query, setQuery] = useState('');
  const [accessToken, setAccessToken] = useState('');
  const [isMounted, setIsMounted] = useState();
  const [date, setDate] = useState([null, null]);
  const [doRequest, setDoRequest] = useState(false);

  const getUserMessages = async () => {
    if (accessToken !== '' && !lastPage) {
      setLoading(true);
      axios
        .get(`${MESSAGE_URL}/${user.user.id}/messages`, {
          params: {
            key: API_KEY,
            maxResults: 20,
            q: `${
              date[0] && date[1]
                ? 'after:' + date[0] + ' before:' + date[1]
                : ''
            }${query ? ' ' + query : ''}`,
            includeSpamTrash: false,
            ...(nextPageToken && {pageToken: nextPageToken}),
          },
          cancelToken: source.token,
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        })
        .then(({data}) => {
          console.log(data);
          if (data.nextPageToken) {
            setNextPageToken(data.nextPageToken);
          } else {
            setLastPage(true);
            setNextPageToken(null);
          }
          data.messages &&
            data.messages.map(message => {
              return axios
                .get(`${MESSAGE_URL}/${user.user.id}/messages/${message.id}`, {
                  params: {key: API_KEY},
                  headers: {
                    Authorization: `Bearer ${accessToken}`,
                  },
                  cancelToken: source.token,
                })
                .then(response => {
                  if (isMounted) {
                    setMessages(prev => [...prev, response.data]);
                  }
                })
                .catch(error => {
                  console.error(error);
                });
            });
          setLoading(false);
        })
        .catch(error => {
          setLoading(false);
          console.error(error);
        });
    }
  };

  const refreshHandler = () => {
    setLastPage(false);
    setNextPageToken(null);
    setMessages([]);
    setDoRequest(true);
  };

  const searchHandler = (query, range) => {
    setLastPage(false);
    setMessages([]);
    setNextPageToken(null);
    setQuery(query);
    setDate(range);
    setDoRequest(true);
  };

  const getAccessToken = async () => {
    await GoogleSignin.getTokens().then(response => {
      if (isMounted) {
        setAccessToken(response.accessToken);
        setDoRequest(true);
      }
    });
  };

  useEffect(() => {
    setIsMounted(true);
    setDoRequest(true);
    return () => {
      setIsMounted(false);
    };
  }, []);

  useEffect(() => {
    if (isMounted) {
      getAccessToken();
    }
  }, [isMounted]);

  useEffect(() => {
    if (doRequest) {
      getUserMessages();
      setDoRequest(false);
    }
  }, [doRequest]);

  return (
    <View>
      <View style={style.profileContainer}>
        <View style={style.profileInfoSide}>
          <View>
            <Image
              style={{width: 100, height: 100}}
              source={{uri: user.user.photo}}
            />
          </View>
          <View>
            <Text>{user.user.name}</Text>
            <Text>{user.user.email}</Text>
          </View>
        </View>
        <View style={style.buttonSide}>
          <Button
            style={style.signOutButton}
            color={Colors.primary}
            onPress={signOut}
            title="Signout"
          />
        </View>
      </View>
      <View style={style.listContainer}>
        <Search
          query={query}
          disable={loading}
          date={date}
          searchHandler={searchHandler}
        />
        <FlatList
          data={messages}
          refreshing={loading}
          renderItem={ListItem}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={ItemSeprator}
          refreshControl={
            <RefreshControl
              colors={['#9Bd35A', '#689F38']}
              refreshing={loading}
              onRefresh={refreshHandler}
            />
          }
          onEndReached={getUserMessages}
        />
      </View>
    </View>
  );
}
const style = StyleSheet.create({
  listContainer: {
    marginTop: 6,
    marginHorizontal: 6,
  },
  profileContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileInfoSide: {
    flex: 3,
    padding: 3,
  },

  buttonSide: {
    flex: 1,
    padding: 3,
  },
  signOutButton: {
    fontSize: 7,
  },
});
