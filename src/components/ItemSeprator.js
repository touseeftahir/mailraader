import React from 'react';
import {View, StyleSheet} from 'react-native';
export function ItemSeprator() {
  return <View style={style.seprator} />;
}
const style = StyleSheet.create({
  seprator: {
    height: 2,
    width: '100%',
    backgroundColor: '#CED0CE',
  },
});
