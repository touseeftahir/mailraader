import React from 'react';
import {Text, View} from 'react-native';
import {Base64} from 'js-base64';
export function ListItem({item}) {
  const headers = item.payload.headers.reduce(
    (obj, item) => Object.assign(obj, {[item.name]: item.value}),
    {},
  );

  // console.log(item.payload.parts);
  let parts = (item.payload.parts || []).filter(
    part =>
      (part.mimeType === 'text/plain' || part.mimeType === 'text/html') &&
      part.filename === '',
  );
  let body = '';
  if (item.payload.data) {
    body = Base64.decode(item.payload.body.data);
  } else if (parts.length) {
    body = Base64.decode(parts[0].body.data);
  } else {
    body = item.snippet;
  }
  return (
    <View>
      <Text>Subject: {headers.Subject}</Text>
      <Text>From: {headers.From}</Text>
      <Text>Date: {headers.Date}</Text>
      <Text>Body: {body}</Text>
    </View>
  );
}
