import React, {useState} from 'react';
import {TextInput, StyleSheet, View, Button} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
export function Search({searchHandler, query, date, disable}) {
  const [search, setSearch] = useState(query);
  const [range, setRange] = useState(date);

  return (
    <View>
      <View>
        <View>
          <TextInput
            value={search}
            onChangeText={setSearch}
            placeholder="Search"
            style={style.search}
          />
        </View>
        <View style={style.inputContainer}>
          <View style={style.input}>
            <TextInput
              value={range[0]}
              onChangeText={text => setRange([text, range[1]])}
              placeholder="YYYY/MM/DD"
              style={style.search}
            />
          </View>
          <View style={style.input}>
            <TextInput
              value={range[1]}
              onChangeText={text => setRange([range[0], text])}
              placeholder="YYYY/MM/DD"
              style={style.search}
            />
          </View>
        </View>
      </View>
      <Button
        title="Find"
        color={disable ? Colors.gray : Colors.primary}
        disabled={disable}
        onPress={() => {
          searchHandler(search, range);
        }}
      />
    </View>
  );
}
const style = StyleSheet.create({
  search: {
    borderRadius: 5,
    borderColor: '#CED0CE',
    borderWidth: 2,
    marginBottom: 5,
  },
  inputContainer: {
    flexDirection: 'row',
    marginBottom: 5,
  },
  input: {
    flex: 1,
    padding: 2,
  },
});
